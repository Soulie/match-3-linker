﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using Vectrosity;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// The GridManager manages the game logic and the state of the board.
/// The board origin is bottom left and gems are stored with the following coordinates:
///     
///     0,3  1,3  2,3  3,3 
///     0,2  1,2  2,2  3,2 
///     0,1  1,1  2,1  3,1 
///     0,0  1,0  2,0  3,0 
/// </summary>
public class GridManager : MonoBehaviour 
{
	[SerializeField] 
    private LevelData _levelData;

    [SerializeField] 
    private GameObject _gemPrefab;

    // Represents how tightly the grid fits to the screen's width (1 = perfect fit)
    [SerializeField]
    private float _gridToScreenRatio = 0.8f;

    /// <summary>
    /// Represents the current state of the game board.
    /// </summary>
    private Gem[,] board;
    
    private SelectionManager selectionManager;

    private void Awake()
    {
        board = new Gem[_levelData.NumColumns, _levelData.NumRows];
        selectionManager = GetComponent<SelectionManager>();
    }

    private void Start () 
    {
        AdjustToResolution();

        CreateNewGemBoard();

        Messenger.AddListener(ListenerMethods.TimeUp, TimeUp);
        Messenger.AddListener(ListenerMethods.AdjustToResolution, AdjustToResolution);

        selectionManager.Init(FillGapsInBoard);
    }    

    private void OnDestroy()
    {
        Messenger.RemoveListener(ListenerMethods.TimeUp, TimeUp);
        Messenger.RemoveListener(ListenerMethods.AdjustToResolution, AdjustToResolution);
    }	

    private void AdjustToResolution()
    {
        float camHeight = 2 * Camera.main.orthographicSize;
        float camWidth = camHeight * Camera.main.aspect;

        // Use the camera width to get the width of each square
        float halfNumColumns = _levelData.NumColumns / 2f;
        float halfNumRows = _levelData.NumRows / 2f;
        float squareWidth = (camWidth / _levelData.NumColumns) * _gridToScreenRatio;
        float halfSquareWidth = squareWidth * 0.5f;

        // Resize accordingly
        transform.localScale = Vector3.one * squareWidth;

        // Centre the grid
        float gridX = -halfNumColumns * squareWidth + halfSquareWidth;
        float gridY = -halfNumRows * squareWidth + halfSquareWidth;
        transform.position = new Vector2(gridX, gridY);
    }

    private Gem Get(int row, int column)
    {
        return board[row, column];
    }

    private void CreateNewGemBoard()
    {
        for (int row = 0; row < _levelData.NumColumns; row++)
        {
            for (int column = 0; column < _levelData.NumRows; column++)
            {
				// Spawn a brand new gem.
				// This only sets the data in the board array and does not trigger animations.
                SpawnNewGem(row, column);
            }
        }

        // Now we have a new board created, we can fly the gems in.
        StartCoroutine(AnimateFallingGems());
    }

    private void SpawnNewGem(int row, int column)
    {
        // Spawn a gem with the grid as it's parent, as the correct row and column, but increased column height so it can later fall down.
        Gem gem = _gemPrefab.Spawn(transform, new Vector2(row, column + 10)).GetComponent<Gem>();
        gem.Init(_levelData.GetRandomGemData());
        gem.SetRowColumn(row, column + 10);

        // Keep the gem at scale of 1
        gem.transform.localScale = Vector3.one;

        // Save this gem at position.
        board[row, column] = gem;
    }

    private IEnumerator AnimateFallingGems(Action callback) 
    {
        for (int row = 0; row < _levelData.NumColumns; row++)
        {
            for (int column = 0; column < _levelData.NumRows; column++)
            {
                // Grab the gem at this row, column.
                Gem gem = Get(row, column);
                if (gem != null)
                {
                    // Tell it it's correct row and column so it begins to animate into place - the gem handles this.
                    gem.SetRowColumn(row, column);
                    // Delay next iteration to get a nice falling effect.
                    yield return new WaitForEndOfFrame();
                }
            }
        }

		// If we got passed a callback, invoke it
        callback?.Invoke();
    }

	private IEnumerator AnimateFallingGems() 
	{
		return AnimateFallingGems(null);
	}

    /// <summary>
    /// After we've cleared some gems, there will be holes. 
    /// This method plugs those holes by moving gems down the board
    /// to occupy and spaces below, leaving the holes at the top of the board.
    /// </summary>
    private void FillGapsInBoard()
    {
		// First, update the board state to move all holes to the top of the board.
        for (int row = 0; row < _levelData.NumColumns; row++)
        {
            for (int column = 0; column < _levelData.NumRows; column++)
            {
                // If this tile is empty
                if (Get(row, column) == null)
                {
                    // Check the gem above
                    for (int lookup = column + 1; lookup < _levelData.NumRows; lookup++)
                    {
                        // If we got a gem above
                        Gem gem = Get(row, lookup);
                        if (gem != null)
                        {
                            // Move the gem down a position
                            board[row, lookup] = null;
                            board[row, column] = gem;
                            break;
                        }
                    }
                }
            }
        }

        // Now the board state is updated, animate gems falling into new position.
		// Once this is done, spawn new gems into the gaps at the top.
		StartCoroutine(AnimateFallingGems(SpawnGemsForGaps));
    }

    private void SpawnGemsForGaps()
    {
		// First update the board data model for empty positions.
        for (int row = 0; row < _levelData.NumColumns; row++)
        {
            for (int column = _levelData.NumRows - 1; column >= 0 && Get(row, column) == null; column--)
            {
                SpawnNewGem(row, column);
            }   
        }

        // Animate the new gems falling into place.
        StartCoroutine(AnimateFallingGems());
    }

    private void TimeUp()
    {
        // Stop all animations (i.e. coroutines)
        StopAllCoroutines();
    }
}
