﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// The LevelManager loads the attached scene.
/// </summary>
public class LevelManager : MonoBehaviour
{
    [SerializeField]
    private Object _gameScene;

    public void ChangeLevel()
    {
        SceneManager.LoadScene(_gameScene.name);
    }
}
