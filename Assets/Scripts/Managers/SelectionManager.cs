﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vectrosity;

/// <summary>
/// Responsible for the currently selected match/chain of gems.
/// Highlights the appropriate gems, draw lines between them and display a score on successful clearing of the chain.
/// </summary>
public class SelectionManager : MonoBehaviour
{
    [SerializeField]
    private Material _lineMaterial;
    [SerializeField]
    private RectTransform _uiTransform;
    [SerializeField]
    private ScoreText _scorePrefab;
    [SerializeField]
    private ExtraTimeText _extraTimePrefab;
    [SerializeField]
    private LevelData _levelData;

    private VectorLine _vectorLine;
    private Camera _mainCamera;

    /// <summary>
    /// Stack of gem positions on board (x, y) to represent current selection swipe.
    /// </summary>
    private readonly LinkedList<Gem> _selection = new LinkedList<Gem>();

    private Action selectionClearedCallback;

    private void Awake()
    {
        _vectorLine = new VectorLine("Line", new List<Vector2>(), _lineMaterial, 5.0f);
        _mainCamera = Camera.main;
    }

    private void Start()
    {
        Messenger.AddListener(ListenerMethods.TimeUp, ClearSelection);
    }

    public void Init(Action callback)
    {
        // Set a callback to trigger when chain cleared.
        selectionClearedCallback = callback;
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(ListenerMethods.TimeUp, ClearSelection);

        StopAllCoroutines();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            // Perform raycast to see if we are touching anything at our current touch position (each gem has a collider to react to this).
            RaycastHit2D hit = Physics2D.Raycast(_mainCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            // If we are touching something.
            if (hit.transform != null)
            {
                // Load the gem (only thing it can be in this game).
                Gem selectedGem = hit.transform.gameObject.GetComponent<Gem>();

                // See what we can do with it.
                NewGemSelected(selectedGem);
            }

            // Always update the matched-chain as long as the user is dragging
            UpdateSelectedChainedGems();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            // Mouse button released so process the current chain to see if we get score.
            ProcessSelection();
        }
    }

    private void NewGemSelected(Gem selectedGem)
    {
        // If our stack is empty, add it immediately since it's the first in the chain.
        if (_selection.Count == 0)
        {
            AddGemToSelection(selectedGem);
        }
        else
        {
            // Otherwise, check the previous gem in our chain.
            Gem previousGem = _selection.Last();

            // If it's not the same gem we're touching (user is holding finger down on same tile)
            // and it's the same coloured gem 
            if (previousGem != null && !selectedGem.Equals(previousGem) && selectedGem.Name == previousGem.Name)
            {
                // If we have previously touched it then we're moving back in the chain
                if (_selection.Contains(selectedGem))
                {
                    RemoveGemsFromSelection(selectedGem);
                }
                // Otherwise we haven't previously touched it
                // and if it's a neighbour then we can add it to our selection
                else if (IsNeighbourOfPrevious(selectedGem))
                {
                    AddGemToSelection(selectedGem);
                }
            }
        }
    }

    private void AddGemToSelection(Gem gem)
    {
        _selection.AddLast(gem);
    }

    private void RemoveGemsFromSelection(Gem selectedGem)
    {
        // If this mode allows us to undo selection
        if (_levelData.CanUndoSelection == true)
        {
            // Remove all gems from the chain that are after the selected gem
            while (_selection.Last.Value != selectedGem)
                _selection.RemoveLast();
        }
    }

    /// <summary>
    /// Are the two gems neighbours? This only checks board positions
    /// in every direction (including diagonals), but not colour.
    /// </summary>
    private bool IsNeighbourOfPrevious(Gem selectedGem)
    {
        Gem previous = _selection.Last();

        // If we're only 0 or 1 tiles away from each other then we're neighbours.
        return (Mathf.Abs(selectedGem.Row - previous.Row) <= 1 && Mathf.Abs(selectedGem.Column - previous.Column) <= 1);
    }

    /// <summary>
    /// Shows highlights and lines.
    /// </summary>
    public void UpdateSelectedChainedGems()
    {
        // Always clear our lines.
        _vectorLine.points2.Clear();
        for (int i = 0; i < _selection.Count; i++)
        {
            // Load the gem and highlight it
            Gem gem = _selection.ElementAt(i);
            if (gem != null)
            {
                gem.Highlight();
                if ((i + 1) < _selection.Count)
                {
                    // If we can, draw a line from current gem to next gem.
                    Gem next = _selection.ElementAt(i + 1);
                    if (next != null)
                    {
                        _vectorLine.points2.Add(_mainCamera.WorldToScreenPoint(gem.transform.position));
                        _vectorLine.points2.Add(_mainCamera.WorldToScreenPoint(next.transform.position));
                    }
                }
            }
        }
        // Always call draw or the line won't render.
        _vectorLine.Draw();
    }

    private int CalculateBonusValue(int gemIndex, Gem gem)
    {
        // If this mode allows, apply bonus points to longer links
        if (_levelData.BonusValue > 0)
        {
            int gemCount = gemIndex + 1;
            var minLinksForBonus = _levelData.MinLinksForBonus;
            var bonusValue = _levelData.BonusValue;

            // Increase bonus for each successive link and then return it
            if (gemCount >= minLinksForBonus)
                return bonusValue * (gemCount - (minLinksForBonus - 1));
        }

        // Otherwise return 0
        return 0;
    }

    private int CalculateExtraTime(int count)
    {
        var minLinksForBonus = _levelData.MinLinksForBonus;
        int bonusTime = 1;
        int totalTime = 0;

        // Add up all the bonus times, with each successive link rewarding more
        if (count >= minLinksForBonus)
        {
            for (int i = minLinksForBonus; i <= count; i++)
                totalTime += bonusTime * (i - (minLinksForBonus - 1));
            return totalTime;
        }

        // Otherwise return 0
        return 0;
    }

    private void ProcessSelection()
    {
        if (IsValidSelection(_selection.Count))
        {
            // We cleared some gems!
            // Fade these gems out and remove from board.
            // Once complete, plug the gaps in the board.
            StartCoroutine(ProcessSuccessfulSelection());
        }
        else
        {
            ClearSelection();
        }
    }

    private bool IsValidSelection(int chainLength)
    {
        return chainLength >= _levelData.MinGemsForMatch;
    }   

    private IEnumerator ProcessSuccessfulSelection()
    {
        float fadeDuration = 0.0f;
       
        for (int i = 0, count = _selection.Count; i < count; i++)
        {
            // Load gem to use it's position for score.
            Gem gem = _selection.ElementAt(i);
            
            if (gem != null)
            {
                // Calculate score for this gem
                int score = gem.ScoreValue + CalculateBonusValue(i, gem);

                // Inform whoever cares about new score.
                Messenger.Broadcast(ListenerMethods.AddScore, score);
                
                // Calculate the score text position
                Vector2 scoreTextPosition = _mainCamera.WorldToScreenPoint(gem.transform.position);

                // Spawn the score text at the cleared position and show the score.
                ScoreText scoreText = _scorePrefab.Spawn(scoreTextPosition);
                scoreText.transform.SetParent(_uiTransform);
                scoreText.SetScore(score);

                // Fade gem out
                gem.Clear(i);
                fadeDuration = gem.GetFadeOutDuration();

                // If this isn't the last gem in the chain, cause a slight delay for animation purposes
                if (i != count - 1)
                {
                    yield return new WaitForEndOfFrame();
                }
            }
        }

        // If this mode allows, get extra time for longer links
        if (_levelData.CanGetExtraTime)
        {
            // Calculate any bonus time if mode allows
            int extraTime = CalculateExtraTime(_selection.Count);

            if (extraTime > 0)
            {
                // Inform whoever cares about the extra time.
                Messenger.Broadcast(ListenerMethods.AddTime, extraTime);

                // Calculate the time text position
                Vector2 timeTextPosition = Vector2.one;

                // Spawn the extraTime text next to the timer to show the time gained.
                ExtraTimeText timeText = Instantiate(_extraTimePrefab, _extraTimePrefab.transform.position, Quaternion.identity);
                timeText.transform.SetParent(_uiTransform, false);
                timeText.SetExtraTime(extraTime);
            }

        }

        // Wait for all gems to fade 
        yield return new WaitForSeconds((fadeDuration * .6f) * _selection.Count);
        
        ClearSelection();

		selectionClearedCallback.Invoke();
    }
    
    private void ClearSelection()
    {
        // Clear our gem list
        _selection.Clear();
        // Deselect all gems
        Messenger.Broadcast(ListenerMethods.DisableHighlight);
        // Clear the drawn lines
        _vectorLine.points2.Clear();
        _vectorLine.Draw();
    } 
}
