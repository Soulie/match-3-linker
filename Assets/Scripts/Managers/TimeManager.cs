﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// TimeManager is responsible for ticking down the level clock.
/// </summary>
public class TimeManager : MonoBehaviour 
{
	[SerializeField]
    private LevelData _levelData;

    private int _levelTime;
    private Text _timerText;

	private void Awake()
	{
		_timerText = GetComponent<Text>();
	}
	
	private void Start()
	{
        // Add time listener
        Messenger.AddListener<int>(ListenerMethods.AddTime, AddTime);

        _levelTime = _levelData.Time;
		_timerText.text = _levelTime.ToString();
	    
	    // Start the clock.
        InvokeRepeating(nameof(Countdown), 1.0f, 1.0f);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<int>(ListenerMethods.TimeUp, AddTime);
    }

    /// <summary>
    /// Triggered every second to count down the clock.
    /// </summary>
    private void Countdown()
    {
        // Count down every second
        _levelTime--;

		_timerText.text = _levelTime.ToString();
        
        // If we hit zero...
        if (_levelTime <= 0)
        {
            // Time up!
            CancelInvoke();  

            // Let other things know
            Messenger.Broadcast(ListenerMethods.TimeUp);          
        }
    }

    private void AddTime (int seconds)
    {
        _levelTime += seconds;
    }
}
