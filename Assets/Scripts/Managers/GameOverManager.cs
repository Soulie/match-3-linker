﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour 
{
    private Animator _gameOverAnim;
    private AudioSource _explosion;

    private static readonly int HighScore = Animator.StringToHash("NewHighScore");
    private static readonly int GameOver = Animator.StringToHash("GameOver");

    private void Awake()
    {
        _gameOverAnim = GetComponent<Animator>();
        _explosion = GetComponent<AudioSource>();
    }

    private void Start () 
    {
        Messenger.AddListener(ListenerMethods.TimeUp, TimeUp);
        Messenger.AddListener(ListenerMethods.NewHighScore, NewHighScore);
	}

    private void OnDestroy()
    {
        Messenger.RemoveListener(ListenerMethods.TimeUp, TimeUp);
        Messenger.RemoveListener(ListenerMethods.NewHighScore, NewHighScore);
    }

    private void TimeUp()
    {
        StartCoroutine(ShowGameOver());
    }

    private IEnumerator ShowGameOver()
    {
        // Play explosion sound
        _explosion.Play();
        // Wait for gems to clear screen
        yield return new WaitForSeconds(2.0f);
        // Trigger game over animation
        _gameOverAnim.SetTrigger(GameOver);
    }

    private void NewHighScore()
    {
        StartCoroutine(ShowNewHighScore());
    }

    private IEnumerator ShowNewHighScore()
    {
        yield return new WaitForSeconds(3.0f);
        _gameOverAnim.SetTrigger(HighScore);
    }

    /// <summary>
    /// Triggered from Button to play the game again.
    /// </summary>
    public void PlayAgain()
    {
        // Load the level again
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Triggered by Button to return to menu
    /// </summary>
    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
