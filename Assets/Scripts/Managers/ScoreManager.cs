﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Manages the current score and highscore values.
/// </summary>
public class ScoreManager : MonoBehaviour 
{
    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private Text _highScoreText;

    /// <summary>
    /// High score is saved to PlayerPrefs so it persists between plays.
    /// </summary>

    private int _highScore;
    private string _currentSceneName;

    /// <summary>
    /// The current score.
    /// </summary>
    private int score;

    private void Start()
    {
        // Reset score.
        score = 0;

        // Get current scene name
        _currentSceneName = SceneManager.GetActiveScene().name;

        // Get current highscore
        _highScore = PlayerPrefs.GetInt(_currentSceneName);

        // Add score & time listeners
        Messenger.AddListener<int>(ListenerMethods.AddScore, UpdateScore);
        Messenger.AddListener(ListenerMethods.TimeUp, TimeUp);

        _highScoreText.text = _highScore.ToString();
    }

    private void Update()
    {
        _scoreText.text = score.ToString();
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<int>(ListenerMethods.AddScore, UpdateScore);
        Messenger.RemoveListener(ListenerMethods.TimeUp, TimeUp);
    }

    private void UpdateScore(int points)
    {
        score += points;
    }

    private void TimeUp()
    {
        // Check if we got a new high score.
        if (score > _highScore)
        {
            _highScore = score;
            _highScoreText.text = _highScore.ToString();

            // Update PlayerPrefs so that the new highscore persists
            if (_currentSceneName != null)
                PlayerPrefs.SetInt(_currentSceneName, _highScore);

            // Send message to notify game over screen
            Messenger.Broadcast(ListenerMethods.NewHighScore);
        }
    }
}
