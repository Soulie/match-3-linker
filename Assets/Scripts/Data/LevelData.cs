using UnityEngine;

/// <summary>
/// Holds all Level-related data
/// </summary>
[CreateAssetMenu]
public class LevelData : ScriptableObject
{
    [SerializeField] private int _numRows;
    [SerializeField] private int _numColumns;
    
    [SerializeField] private GemData[] _gems;

    [SerializeField] private int _minGemsForMatch;

    [SerializeField] private int _time;

    [SerializeField] private int _bonusValue;

    [SerializeField] private int _minLinksForBonus;

    [SerializeField] private bool _canUndoSelection;

    [SerializeField] private bool _canGetExtraTime;

    public int NumRows => _numRows;

    public int NumColumns => _numColumns;

    public GemData GetRandomGemData()
    {
        return _gems[Random.Range(0, _gems.Length)];
    }

    public int MinGemsForMatch => _minGemsForMatch;

    public int Time => _time;

    public int BonusValue => _bonusValue;

    public int MinLinksForBonus => _minLinksForBonus;

    public bool CanUndoSelection => _canUndoSelection;

    public bool CanGetExtraTime => _canGetExtraTime;
}