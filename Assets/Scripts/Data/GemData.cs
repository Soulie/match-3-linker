using UnityEngine;

/// <summary>
/// Holds all Gem-related data
/// </summary>
[CreateAssetMenu]
public class GemData : ScriptableObject
{
    [SerializeField] private string _name;
    
    [SerializeField] private Sprite _spriteDefault;
    [SerializeField] private Sprite _spriteHighlighted;

    [SerializeField] private int _scoreValue;

    public string Name => _name;
    
    public Sprite SpriteDefault => _spriteDefault;

    public Sprite SpriteHighlighted => _spriteHighlighted;

    public int ScoreValue => _scoreValue;
}
