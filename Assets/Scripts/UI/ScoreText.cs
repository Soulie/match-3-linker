﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ScoreText is generated once a gem is cleared
/// in order to show the user in place how many points
/// that gem was worth.
/// </summary>
[RequireComponent(typeof(Text))]
public class ScoreText : MonoBehaviour 
{
    [SerializeField]
    private float _startScale = 0.4f;
    [SerializeField]
    private float _endScale = 1.0f;

    [SerializeField]
    private float _speedY = 10.0f;
    [SerializeField]
    private float _duration = 1.0f;

    private float _timer;
    private Text _scoreText;

    private void OnEnable()
    {
        _timer = 0.0f;
        _scoreText = GetComponent<Text>();
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        float t = Mathf.Clamp01(_timer / _duration);
        float scale = Mathf.Lerp(_startScale, _endScale, t);
        float alpha = 1.0f - t;

        // Scale up
        transform.localScale = new Vector3(scale, scale, 1.0f);
        // Float up
        transform.position += new Vector3(0.0f, _speedY * Time.deltaTime, 0.0f);
        // Fade out
        _scoreText.color = new Color(1.0f, 1.0f, 1.0f, alpha);

        // When time's up, recycle the object.
        if (_timer > _duration)
        {
            gameObject.Recycle();
        }
    }

    public void SetScore(int score)
    {
        _scoreText.text = score.ToString();
    }
}
