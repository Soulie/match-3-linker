﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ExtraTimeText is generated once a whole chain is cleared
/// in order to show the user how many seconds of extra time
/// they have gained.
/// </summary>
[RequireComponent(typeof(Text))]
public class ExtraTimeText : MonoBehaviour
{
    [SerializeField]
    private float _startScale = 0.4f;
    [SerializeField]
    private float _endScale = 1.5f;

    [SerializeField]
    private float _speedY = 10.0f;
    [SerializeField]
    private float _duration = 1.0f;

    private float _timer;
    private Text _extraTimeText;

    private void OnEnable()
    {
        _timer = 0.0f;
        _extraTimeText = GetComponent<Text>();
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        float t = Mathf.Clamp01(_timer / _duration);
        float scale = Mathf.Lerp(_startScale, _endScale, t);
        float alpha = 1.0f - t;

        // Scale up
        transform.localScale = new Vector3(scale, scale, 1.0f);
        // Float up
        transform.position += new Vector3(0.0f, _speedY * Time.deltaTime, 0.0f);
        // Fade out
        _extraTimeText.color = new Color(1.0f, 1.0f, 1.0f, alpha);

        // When time's up, recycle the object.
        if (_timer > _duration)
        {
            gameObject.Recycle();
        }
    }

    public void SetExtraTime(int extraTime)
    {
        _extraTimeText.text = "+ " + extraTime.ToString();
    }
}
