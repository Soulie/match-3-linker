﻿using System;

/// <summary>
/// Contains some string constants to improve type-safety of listeners.
/// </summary>
public static class ListenerMethods
{
    public const string DisableHighlight = "DisableHighlight";
    public const string AddScore = "AddScore";
    public const string TimeUp = "TimeUp";
    public const string AddTime = "AddTime";
    public const string NewHighScore = "NewHighScore";
    public const string AdjustToResolution = "AdjustToResolution";
}

