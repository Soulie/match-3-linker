﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The ScaleCalculator finds out how much we need to scale our sprites
/// in accordance with different aspect ratios and only needs to be run once
/// at the beginning of the game.
/// </summary>
public class ScaleCalculator : MonoBehaviour
{
    public static ScaleCalculator Instance;
    private int _displayWidth, _displayHeight, _oldDisplayWidth, _oldDisplayHeight;

    private void Awake()
    {
        // Create a singleton for global access
        #region Singleton

        Instance = this;

        #endregion

        _displayWidth = Screen.width;
        _displayHeight = Screen.height;

        // Allow this object to persist
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        UpdateDisplayDimensions();

        // Check if resolution has changed then adjust the scale accordingly
        if (_displayWidth != _oldDisplayWidth || _displayHeight != _oldDisplayHeight)
        {
            //GetScreenScale();
            Messenger.Broadcast(ListenerMethods.AdjustToResolution);
        }
    }

    // Get the current device's display properties
    private void UpdateDisplayDimensions()
    {
        _oldDisplayWidth = _displayWidth;
        _oldDisplayHeight = _displayHeight;
        _displayWidth = Screen.width;
        _displayHeight = Screen.height;
    }
}