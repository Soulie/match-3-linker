﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// OneTimeSetup sets up any variables/data
/// that need to be initialized before the game starts.
/// </summary>
public class OneTimeSetup : MonoBehaviour
{
    [SerializeField]
    private Object[] _gameModes;

    // Start is called before the first frame update
    void Start()
    {
        // Initialize highscores for each game mode
        for (int i = 0; i < _gameModes.Length; i++)
        {
            PlayerPrefs.SetInt(_gameModes[i].name, 0);
        }

        // Go to the Main Menu
        GetComponent<LevelManager>().ChangeLevel();
    }
}
