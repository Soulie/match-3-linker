﻿using UnityEngine;

/// <summary>
/// Always attempts to move the gem towards the correct position (row, column) on the board.
/// </summary>
public class GemMover : MonoBehaviour 
{
	[SerializeField]
    private float _fallSpeed = 12.0f;
    
    /// <summary>
    /// Try to move the gem towards it's tile. This only occurs on spawn
    /// when the gem is floating down into it's place.
    /// </summary>
    public void TryMoveTowardsTile(int row, int column)
    {
        // If we're not at the correct tile position, move towards it.
        if (transform.localPosition.y != column)
        {
			transform.localPosition = Vector2.Lerp(transform.localPosition, new Vector2(row, column), _fallSpeed * Time.deltaTime);
        }
    }
}
