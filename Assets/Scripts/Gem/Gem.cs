﻿using UnityEngine;

public class Gem : MonoBehaviour
{
    /// <summary>
    /// The column index of this gem on the board.
    /// </summary>
    public int Column { get; private set; }

    /// <summary>
    /// The row index of this gem on the board
    /// </summary>
    public int Row { get; private set; }

    private enum GemState
    {
        Active,
        Cleared,
        TimeUp
    }

    private GemState _gemState = GemState.Active;

    private GemHighlighter _highlighter;
    private GemFader _fader;
    private GemMover _mover;

    private GemData _gemData;

    private void Awake()
    {
        _highlighter = GetComponent<GemHighlighter>();
        _fader = GetComponent<GemFader>();
        _mover = GetComponent<GemMover>();
    }

    private void Start()
    {        
        Messenger.AddListener(ListenerMethods.TimeUp, TimeUp);
    }
    
    private void OnDestroy()
    {
        Messenger.RemoveListener(ListenerMethods.TimeUp, TimeUp);
    }

    public void Init(GemData gemData)
    {
        _gemData = gemData;
        _highlighter.Init(gemData.SpriteDefault, gemData.SpriteHighlighted);
    }

    private void Update()
    {
        switch (_gemState)
        {
            case GemState.Active:
                _mover.TryMoveTowardsTile(Row, Column);
                break;

            case GemState.Cleared:
                _fader.Fade();
                break;

            case GemState.TimeUp:
                break;
        }
    }
    
    private void OnBecameInvisible()
    {
        // If time is up and off screen, recycle the object
        if (_gemState == GemState.TimeUp)
        {
            gameObject.Recycle();
        }
    }

    /// <summary>
    /// Switches the current gem state if it's valid.
    /// </summary>
    private void SwitchState(GemState newState)
    {
        if (_gemState == GemState.Active)
        {
            _gemState = newState;
        }
    }
    
    /// <summary>
    /// Sets the row and column index of the gem on the board.
    /// </summary>
    public void SetRowColumn(int row, int column)
    {
        Row = row;
        Column = column;
    }

    public void Highlight()
    {
        _highlighter.Highlight();
    }

    public void DisableHighlight()
    {
        _highlighter.DisableHighlight();
    }

    public float GetFadeOutDuration()
    {
        return _fader.FadeOutDuration;
    }

    public void Clear(int chainIndex)
    {
        // Pitch up audio based on position in chain (zero-index so + 1).
        // If it's the first item, don't pitch it up. Otherwise pitch up by 15%.
        _fader.ChimePitch = (chainIndex + 1) == 1 ? 1 : 1 + (chainIndex * .15f);

        // Fade this gem out
        SwitchState(GemState.Cleared);
    }

    private void TimeUp()
    {
        SwitchState(GemState.TimeUp);
    }

    public string Name => _gemData.Name;
    
    public int ScoreValue => _gemData.ScoreValue;
}
