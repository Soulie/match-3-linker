﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Generic sprite highlighter when the mouse (or touch) is over an object.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class GemHighlighter : MonoBehaviour 
{
    private Sprite _defaultSprite;
    private Sprite _highlightedSprite;
    private SpriteRenderer _spriteRenderer;

    private bool _isHighlighted = false;

    private void Awake () 
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        // Listen for when we want to set ALL gems to not highlighted.
        Messenger.AddListener(ListenerMethods.DisableHighlight, DisableHighlight);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(ListenerMethods.DisableHighlight, DisableHighlight);
    }

    public void Highlight()
    {
        // If not already set, switch sprite.
        if (!_isHighlighted)
        {
            _isHighlighted = true;
            _spriteRenderer.sprite = _highlightedSprite;
        }
    }

    public void DisableHighlight()
    {       
        _spriteRenderer.sprite = _defaultSprite;
        _isHighlighted = false;
    }

    public void Init(Sprite spriteDefault, Sprite spriteHighlighted)
    {
        _defaultSprite = spriteDefault;
        _highlightedSprite = spriteHighlighted;
        
        DisableHighlight();
    }
}
