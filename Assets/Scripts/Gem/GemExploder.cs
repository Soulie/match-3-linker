﻿using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Let's the physics system blast the gem away.
/// Usually only useful once this gem is no longer needed...
/// </summary>
public class GemExploder : MonoBehaviour 
{
    private void Start()
    {
        Messenger.AddListener(ListenerMethods.TimeUp, Explode);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(ListenerMethods.TimeUp, Explode);
    }

    private void Explode()
    {
        // If time is up, let the rigidbody fly the gem off screen by adding random impulse.
        if (gameObject.GetComponent<Rigidbody2D>() == null)
        {
            Rigidbody2D rigidBody = gameObject.AddComponent<Rigidbody2D>();
            rigidBody.AddForce(Random.insideUnitSphere * 15.0f, ForceMode2D.Impulse);
            rigidBody.AddTorque(Random.Range(-120, 120));
        }
    }
}
