﻿using System;
using System.Collections;
using UnityEngine;

public class GemFader : MonoBehaviour
{
    [SerializeField]
    private float _fadeOutDuration = 0.05f;

    private AudioSource _chime;
    private float _timer;
    private Material _material;
    
    private void Awake()
    {
        _material = GetComponent<Renderer>().material;
        _chime = GetComponent<AudioSource>();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    public void Fade()
    {
        // Play chime
        _chime.Play();

        // Reset time to fade.        
        _timer = 0.0f;
        
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        while (_timer < FadeOutDuration)
        {
            // Calculate new fade out time and alpha levels.
            _timer += Time.deltaTime;

            float t = Mathf.Clamp01(_timer / FadeOutDuration);
            float alpha = 1.0f - t;

            _material.color = new Color(1.0f, 1.0f, 1.0f, alpha);

            yield return null;
        }

        gameObject.Recycle();
    }
    
    public float FadeOutDuration => _fadeOutDuration;

    public float ChimePitch
    {
        set => _chime.pitch = value;
    }
}
